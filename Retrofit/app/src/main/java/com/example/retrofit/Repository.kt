package com.example.retrofit

import retrofit2.Response

class Repository {
    suspend fun getPost(): Response<Post> {
       return RetrofitInstance.api.getPost()
    }

    suspend fun getCustomPost(): Response<Post1> {
        return RetrofitInstance.api.getCustomPost()
    }


}