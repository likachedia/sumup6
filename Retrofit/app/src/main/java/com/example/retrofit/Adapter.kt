package com.example.retrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.retrofit.databinding.ModelFragmentBinding


class Adapter(private val items: List<Content>): RecyclerView.Adapter<Adapter.ItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.ItemViewHolder {
        return ItemViewHolder(ModelFragmentBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size
    inner class ItemViewHolder(private val binding: ModelFragmentBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            val model = items[adapterPosition].cover
            binding.simpleText1.text = items[adapterPosition].titleKA
            binding.simpleText2.text = items[adapterPosition].descriptionKA
            binding.simpleText3.text = items[adapterPosition].publish_date
           // Glide.with(itemView.context).load(model).into(binding.img4)
            binding.img4.setImg(itemView.context, model,binding.img4 )
        }
    }

}