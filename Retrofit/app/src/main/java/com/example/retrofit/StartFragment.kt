package com.example.retrofit


import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit.databinding.FragmentStartBinding


class StartFragment : BaseFragment<FragmentStartBinding, PostViewModel>(FragmentStartBinding::inflate) {
    override fun getViewModelClass() = PostViewModel::class.java
    private lateinit var adapter: Adapter
 //   private lateinit var items: MutableList<Data>
    private lateinit var recycler:RecyclerView

    override fun start() {
        val repository = Repository()
        val viewModelFactory =PostViewModelFactory(repository)
  //      items = mutableListOf()
        httpRequest()

    }

    private  fun httpRequest() {
        binding.get.setOnClickListener {

            viewModel.getCustomPost()
            viewModel.myCustomPost.observe(this, { response ->
                if(response.isSuccessful) {
                    Log.i("Post", response.body()?.get(1).toString())
                 //  binding.simpleText.text = response.body()?.toString()
                val items = response.body()?.content
                    binding.recycler.layoutManager = LinearLayoutManager(requireContext())

                    adapter = items?.let { it1 -> Adapter(it1) }!!
                    binding.recycler.adapter = adapter


                } else {
                    Log.i("Post", response.errorBody().toString())
                  //  binding.simpleText.text = response.code().toString()
                }

            })
        }

    }

}
