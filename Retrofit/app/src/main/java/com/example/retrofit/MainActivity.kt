package com.example.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.retrofit.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel:PostViewModel
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val repository = Repository()
        val viewModelFactory =PostViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostViewModel::class.java)



    /*    binding.get.setOnClickListener {
           val myNumber = binding.editTextNumber.text.toString()
            val myPost = Post(2, 2, "lchedia", "Android developer")
            viewModel.pushPost(myPost)
            viewModel.myResponse.observe(this, { response ->
                if(response.isSuccessful) {
                    Log.i("Main", response.body().toString())
                    Log.i("Main", response.code().toString())
                    Log.i("Main", response.message().toString())

                } else {
                    Log.i("Post", response.errorBody().toString())
                    binding.simpleText.text = response.code().toString()
                }

            })
        } */
    }




}