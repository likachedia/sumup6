package com.example.retrofit

import retrofit2.Response
import retrofit2.http.*

interface SimpleApi {
    @GET("/v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab")
    suspend fun getPost(): Response<Post>

    @GET("/v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab")
    suspend fun getCustomPost(
    ): Response<Post1>

}