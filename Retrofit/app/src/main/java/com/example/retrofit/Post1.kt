package com.example.retrofit

data class Post1(
    val content: List<Content>
) {
    operator fun get(i:Int): Content {
        return content[i]
    }
}