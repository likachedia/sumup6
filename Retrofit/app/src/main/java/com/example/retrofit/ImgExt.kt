package com.example.retrofit

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImg(content: Context, model:String, view:ImageView) {
    Glide.with(this).load(model).into(view)
}