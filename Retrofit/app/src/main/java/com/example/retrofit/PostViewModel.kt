package com.example.retrofit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Response

class PostViewModel(private val repository: Repository): BaseViewModel(repository) {

    val myResponse:MutableLiveData<Response<Post>> = MutableLiveData()
    val myResponse2:MutableLiveData<Response<Post>> = MutableLiveData()
    val myCustomPost:MutableLiveData<Response<Post1>> = MutableLiveData()
/*
    fun pushPost(post: Post) {
        viewModelScope.launch {
            val response = repository.pushPost(post)
            myResponse.value = response
        }
    } */

    fun getPost() {
        viewModelScope.launch {
            val response = repository.getPost()
            myResponse.value = response
        }
    }

  /*  fun getPost2(number:Int) {
        viewModelScope.launch {
            val response = repository.getPost2(number)
            myResponse2.value = response
        }
    } */

    fun getCustomPost() {
        viewModelScope.launch {
            val response = repository.getCustomPost()
            myCustomPost.value = response

        }
    }


}