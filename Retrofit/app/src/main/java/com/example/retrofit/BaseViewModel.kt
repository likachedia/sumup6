package com.example.retrofit

import androidx.lifecycle.ViewModel

abstract class BaseViewModel(private val repository: Repository): ViewModel()