package com.example.retrofit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias Inflater<T> = (LayoutInflater, ViewGroup, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding, VM: BaseViewModel>(private val inflate: Inflater<VB>): Fragment() {
    private var _binding:VB? = null
    val binding get() = _binding!!

    open var useSharedViewModel: Boolean = false
    protected lateinit var viewModel: VM

   protected abstract fun getViewModelClass(): Class<VM>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        val repository = Repository()
        val viewModelFactory =PostViewModelFactory(repository)
     //   viewModel = ViewModelProvider(this, viewModelFactory).get(PostViewModel::class.java)
        viewModel = if (useSharedViewModel) {
            ViewModelProvider(requireActivity(), viewModelFactory)[getViewModelClass()]
        } else {
            ViewModelProvider(this, viewModelFactory)[getViewModelClass()]
        }

        start()
        return binding.root
    }

    abstract fun start()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}